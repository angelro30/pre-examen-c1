
const capital = document.querySelector('#capitalResp'),
      lenguaje = document.querySelector('#lenguajeResp');

async function searchCountryByName(){

    const name = document.getElementById('nameCountry').value.trim();
    if(name){
        try{
            const url = `https://restcountries.com/v3.1/name/${name}`;
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(`Error de red: ${response.status}`);
            }
            const data = await response.json();
            capital.innerText = data[0].capital;
            const languages = Object.values(data[0].languages);
            lenguaje.innerText = languages;
        }
            
        catch(error){
            capital.innerText = 'Pais no encontrado...'
        }
    }
}

function limpiarDatos(){
    document.getElementById('nameCountry').value = '';
    capital.innerText = '';
    lenguaje.innerText = '';
}

document.getElementById('btnBuscar').addEventListener('click', searchCountryByName);
document.getElementById('btnLimpiar').addEventListener('click', limpiarDatos);