
const   name = document.querySelector('#nombreResp'),
        user = document.querySelector('#userResp'),
        email = document.querySelector('#emailResp'),
        calle = document.querySelector('#calleResp'),
        num = document.querySelector('#numResp'),
        ciudad = document.querySelector('#ciudadResp');


function buscarUserById(){
    const id = document.getElementById('idUsuario').value.trim();
    if(id){
        const url = `https://jsonplaceholder.typicode.com/users/${id}`;
        axios.get(url)
            .then(response => {
                console.log('Respuesta:', response.data);
                const json = response.data;
                name.innerText   = json.name;
                user.innerText   = json.username;
                email.innerText  = json.email;
                calle.innerText  = json.address.street;
                num.innerText    = json.address.suite;
                ciudad.innerText = json.address.city;
            })
            .catch(error => {
                console.error('Error al realizar la solicitud:', error);
                name.innerText   = 'No se ha encontrado al usuario';
                user.innerText   = '';
                email.innerText  = '';
                calle.innerText  = '';
                num.innerText    = '';
                ciudad.innerText = '';
            });
    }
}

document.getElementById('btnBuscar').addEventListener('click', buscarUserById);