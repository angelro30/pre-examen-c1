
const indexContainer = document.getElementById("index-container");

const nombreDePracticas = [
    "Axios",
    "Fetch",
];

const numeroDePracticas = nombreDePracticas.length;

const renderizarPracticas = () => {
    for (let i = 1; i <= numeroDePracticas; i++) {
      const cardPractica = `
        <div onclick="window.location = 'Practica0${i}/index.html';"
            class="flex flex-col justify-between text-white bg-[rgba(0,0,0,0.65)]
                rounded-lg shadow shadow-[rgba(0,0,0,0.8)] hover:scale-[110%]
                transition-all duration-300">
        <a class="inline-block relative my-2 mx-2 hover:text-indigo-400 transition-all duration-300
                subrayado" href="Practica0${i}/index.html">${i}. ${nombreDePracticas[i-1]}</a>
        <img class="w-full cursor-pointer rounded-lg"
        src="img/practica${i}.jpg">
        </div>
        `;
      indexContainer.innerHTML += cardPractica;
    }
};

renderizarPracticas();


